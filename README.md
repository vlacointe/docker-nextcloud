Docker nextlcoud
================
Docker-compose repository that runs netxtcloud service. This repository needs to be used with [docker-proxy](https://gitlab.com/vlacointe/docker-proxy). 

## Configure cron (DOESN'T WORK YET)

If you want to use cron to launch cron.php every 15minutes, you can add this to the host crontab.

```*/15  *  *  *  * /usr/bin/docker exec -it nextcloud_app_1 su - www-data -s /bin/bash -c "php -f /var/www/html/cron.php"```
